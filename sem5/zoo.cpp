#include <iostream>
#include <string>
#include <typeinfo>

// abstract class Animal
class Animal {
 public:
  virtual void speak() const = 0;
  virtual ~Animal() {};
 protected:
  Animal(const std::string& name) : name_(name) {}
  std::string name_;
};

void Animal::speak() const { std::cout << "???" << std::endl; }

class Dog : public Animal {
 public:
  Dog(const std::string& name) : Animal(name) {}
  void speak() const override { Animal::speak(); }
};

class Cat : public Animal {
 public:
  Cat(const std::string& name) : Animal(name) {};
  void meow() const { speak(); }
  void speak() const override { std::cout << "meow" << std::endl; }
  ~Cat() { std::cout << "Cat died..." << std::endl; }
};

class Cow : public Animal {
 public:
  Cow(const std::string& name) : Animal(name) {};
  void speak() const final override { std::cout << "moo" << std::endl; }
  ~Cow() { std::cout << "Cow died..." << std::endl; }
};

class Fish : public Animal {
 public:
  Fish(const std::string& name) : Animal(name) {};
  ~Fish() { std::cout << "Fish died..." << std::endl; }
};

class SpeakingFish final : public Fish {
 public:
  SpeakingFish(const std::string& name) : Fish(name) {};
  void speak() const override { std::cout << "boop" << std::endl; }
  ~SpeakingFish() { std::cout << "Speaking fish died..." << std::endl; }
};

void report(Animal& animal) {
  try {
    Cat& cat = dynamic_cast<Cat&>(animal);
    cat.meow();
  } catch (const std::bad_cast& ex) {
    std::cout << "No cat&((" << std::endl;
  }
}

void report(Animal* animal) {
  Cat* cat = dynamic_cast<Cat*>(animal);
  if (cat == nullptr) {
    std::cout << "No cat*((" << std::endl;
    return;
  }
  cat->meow();
}

int main() {
  // Cat* cat = new Cat("Barsik");
  // Cow* cow = new Cow("Manya");
  // SpeakingFish* fish = new SpeakingFish("Speaker");

  Cat cat("Barsik");
  Cow cow("Manya");
  SpeakingFish fish("Speaker");

  report(cat);
  report(cow);
  report(fish);

  // Dog dog("bobik");
  // dog.speak();

  return 0;
}