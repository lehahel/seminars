#include <climits>
#include <iostream>

class Granny {
 public:
  void speak() { std::cout << "granny"; }
  void bake() { std::cout << "i'm baking"; }
};

class Mom : public virtual Granny {
 public:
  void speak() { std::cout << "mom"; }
};

class Dad : public virtual Granny {
 public:
  void speak() { std::cout << "dad"; }
};

class Child : public Mom, public Dad {
 public:
  void speak() { std::cout << "child"; }
};

int main() {
  Child child;
  child.bake();
  return 0;
}