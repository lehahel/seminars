#include "datetime.h"

// SECONDS
Seconds::Seconds(size_t seconds) : seconds_(seconds) { /* some code */ }

// // 2
// Seconds::Second(size_t seconds) {
//   seconds_ = seconds;
// }

Seconds::Seconds(Minutes minutes)
    : seconds_(SECONDS_IN_MINUTE * minutes.GetMinutes()) {
  // static constexpr size_t seconds_in_minute = 60;
}

Seconds::Seconds(Hours hours) : seconds_(SECONDS_IN_HOUR * hours.GetHours()) {}

size_t Seconds::GetSeconds() const {
  return seconds_;
}

void Seconds::SetSeconds(size_t seconds) {
  seconds_ = seconds;
}


// MINUTES
Minutes::Minutes(size_t minutes) : minutes_(minutes) {};

Minutes::Minutes(Seconds seconds)
  : minutes_(seconds.GetSeconds() / SECONDS_IN_MINUTE) {};

Minutes::Minutes(Hours hours) : minutes_(hours.GetHours() * MINUTES_IN_HOUR) {}

size_t Minutes::GetMinutes() const {
  return minutes_;
}

void Minutes::SetMinutes(size_t minutes) {
  minutes_ = minutes;
}


// HOURS
Hours::Hours(size_t hours) : hours_(hours) {};

Hours::Hours(Seconds seconds)
  : hours_(seconds.GetSeconds() / SECONDS_IN_HOUR) {}

Hours::Hours(Minutes minutes)
  : hours_(minutes.GetMinutes() / MINUTES_IN_HOUR) {}

size_t Hours::GetHours() const {
  return hours_;
}

void Hours::SetHours(size_t hours) {
  hours_ = hours;
}

// TIME
Time::Time(Hours hours, Minutes minutes, Seconds seconds)
  : hours_(hours), minutes_(minutes), seconds_(seconds) {}

// std::ostream& operator<<(std::ostream& out, const Time& time) {
//   std::string semicolon{":"};
//   out << std::to_string(time.hours_.GetHours()) << semicolon
//       << std::to_string(time.minutes_.GetMinutes()) << semicolon
//       << std::to_string(time.seconds_.GetSeconds());
//   return out;
// }