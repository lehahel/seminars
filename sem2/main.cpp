// #include "datetime.h"
// #include "array.h"
#include "matrix.h"

#include <iostream>

int main() {
  // size_t data;
  // std::cin >> data;

  // std::cout << Seconds(data).GetSeconds() << " seconds" << std::endl
  //           << Minutes(Seconds(data)).GetMinutes() << " minutes" << std::endl
  //           << Hours(Seconds(data)).GetHours() << " hours" << std::endl;

  // size_t h, m, s;
  // std::cin >> h >> m >> s;
  // Hours hours(h);
  // Minutes minutes(m);
  // Seconds seconds(s);

  // Time time(hours, minutes, seconds);

  // std::cout << time << std::endl;
  // Array<int, 4> a{1, 2, 3, 4};

  // for (size_t i = 0; i < 4; ++i) {
  //   std::cout << a[i] << std::endl;
  // }

  // std::cout << a.Size() << std::endl;
  // std::cout << a.Empty() << std::endl;

  Matrix<int, 2, 2> a;
  a(0, 0) = 1;
  a(0, 1) = 2;
  a(1, 0) = 3;
  a(1, 1) = 4;

  for (size_t i = 0; i < 2; ++i) {
    for (size_t j = 0; j < 2; ++j) {
      std::cout << a(i, j) << " ";
    }
    std::cout << std::endl;
  }

  return 0;
}