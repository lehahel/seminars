#pragma once

#include <utility>

template <typename T, size_t N>
struct Array {
  T __buffer__[N];

  constexpr T& operator[](size_t idx) {
    return __buffer__[idx];
  }

  constexpr const T& operator[](size_t idx) const {
    return __buffer__[idx];
  }

  constexpr size_t Size() const {
    return N;
  }

  constexpr bool Empty() const {
    return N == 0u;
  }

  constexpr T& At(size_t idx);
  constexpr const T& At(size_t idx) const;

};
