#pragma once

#include <utility>

template <class T, size_t N, size_t M>
struct Matrix {
  T __buffer__[N][M];

  constexpr size_t RowsNumber() const {
    return N;
  }

  constexpr size_t ColumnsNumber() const {
    return M;
  }

  constexpr T& operator()(size_t n, size_t m) {
    return __buffer__[n][m];
  }

  constexpr const T& operator()(size_t n, size_t m) const {
    return __buffer__[n][m];
  }

};