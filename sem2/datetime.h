#pragma once

#include <utility>
#include <string>

class Minutes;
class Hours;

const size_t SECONDS_IN_MINUTE = 60;
const size_t SECONDS_IN_HOUR = 3600;
const size_t MINUTES_IN_HOUR = 60;

class Seconds {
 public:
  Seconds(size_t seconds);
  Seconds(Minutes minutes);
  Seconds(Hours hours);

  size_t GetSeconds() const;
  void SetSeconds(size_t);
 private:
  size_t seconds_{0};
};

class Minutes {
 public:
  Minutes(size_t minutes);
  Minutes(Seconds seconds);
  Minutes(Hours hours);

  size_t GetMinutes() const;
  void SetMinutes(size_t);
 private:
  size_t minutes_{0};
};

class Hours {
 public:
  Hours(size_t hours);
  Hours(Seconds seconds);
  Hours(Minutes min);

  size_t GetHours() const;
  void SetHours(size_t);
 private:
  size_t hours_{0};
};

// class Time;

// std::ostream& operator<<(std::ostream& out, const Time& time);

class Time {
 public:
  Time(Hours, Minutes, Seconds);
 private:
  Seconds seconds_;
  Minutes minutes_;
  Hours hours_;

  // friend std::ostream& operator<<(std::ostream& out, const Time& time);
};

