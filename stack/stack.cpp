#include <iostream>
#include <stdexcept>
#include <cassert>
#include <utility>

namespace {

template <typename T>
struct Node {
  T data;
  Node<T>* next;
};

} // namespace

template <typename T>
class Stack {
 public:
  Stack();
  Stack(const Stack& stack);
  Stack(Stack&& stack);
  Stack& operator=(const Stack& stack);
  Stack& operator=(Stack&& stack);
  ~Stack();

  void push(const T& item);
  void push(T&& item);
  void pop();
  size_t size() const noexcept;
  bool empty() const noexcept;
  void clear();
  T top() const;

 private:
  Node<T>* head_{nullptr};
  size_t size_{0};
  void __copy__(const Stack& from);
  void __move__(Stack&& from);
};

template <typename T>
void Stack<T>::__copy__(const Stack& from) {
  if (from.size_ == 0 || &from == this) return;
  size_ = from.size_;
  Node<T>* head = from.head_;
  Node<T>* tail = new Node<T>;
  head_ = tail;
  while (head) {
    tail->data = head->data;
    tail->next = head->next ? new Node<T> : nullptr;
    tail = tail->next;
    head = head->next;
  }
}

template <typename T>
void Stack<T>::__move__(Stack&& from) {
  head_ = from.head_;
  size_ = from.size_;
  from.head_ = nullptr;
  from.size_ = 0;
}

template <typename T>
Stack<T>::Stack() = default;

template <typename T>
Stack<T>::Stack(const Stack<T>& stack) {
  __copy__(stack);
}

template <typename T>
Stack<T>::Stack(Stack<T>&& stack) {
  __move__(std::move(stack));
}

template <typename T>
Stack<T>& Stack<T>::operator=(const Stack<T>& stack) {
  clear();
  __copy__(stack);
  return *this;
}

template <typename T>
Stack<T>& Stack<T>::operator=(Stack<T>&& stack) {
  clear();
  __move__(std::move(stack));
  return *this;
}

template <typename T>
Stack<T>::~Stack() {
  clear();
}

template <typename T>
void Stack<T>::push(const T& item) {
  Node<T>* new_node = new Node<T>;
  new_node->data = item;
  new_node->next = head_;
  ++size_;
  head_ = new_node;
}

template <typename T>
void Stack<T>::pop() {
  if (size_ == 0) {
    throw std::runtime_error("Trying to pop empty stack");
  }
  Node<T>* old_node = head_;
  head_ = head_->next;
  --size_;
  delete old_node;
}

template <typename T>
size_t Stack<T>::size() const noexcept {
  return size_;
}

template <typename T>
bool Stack<T>::empty() const noexcept {
  return size_ == 0;
}

template <typename T>
T Stack<T>::top() const {
  if (size_ == 0) {
    throw std::runtime_error("Trying to get the top element of empty stack");
  }
  return head_->data;
}

template <typename T>
void Stack<T>::clear() {
  while (size_ > 0) {
    pop();
  }
}

int main() {
  {
    std::cout << "Test 1 started" << std::endl;
    Stack<int> s;
    for (int i = 1; i < 4; ++i) s.push(i);
    for (int i = 3; i > 0; --i) {
      assert(s.top() == i);
      assert(s.size() == static_cast<size_t>(i));
      s.pop();
    }
    std::cout << "Test 1: ok" << std::endl;
  }

  {
    std::cout << "Test 2 started" << std::endl;
    Stack<int> s;
    for (int i = 1; i < 4; ++i) s.push(i);
    Stack<int> t(s);
    for (int i = 3; i > 0; --i) {
        assert(t.top() == i);
        assert(t.size() == static_cast<size_t>(i));
        t.pop();
    }
    std::cout << "Test 2: ok" << std::endl;
  }

   {
    std::cout << "Test 3 started" << std::endl;
    Stack<int> s;
    for (int i = 1; i < 4; ++i) s.push(i);
    Stack<int> t;
    t = s;
    for (int i = 3; i > 0; --i) {
      assert(t.top() == i);
      assert(t.size() == static_cast<size_t>(i));
      t.pop();
    }
    std::cout << "Test 3: ok" << std::endl;
  }

  {
    std::cout << "Test 4 started" << std::endl;
    Stack<int> s;
    for (int i = 1; i < 4; ++i) s.push(i);
    Stack<int> t(std::move(s));
    for (int i = 3; i > 0; --i) {
      assert(t.top() == i);
      assert(t.size() == static_cast<size_t>(i));
      t.pop();
    }
    std::cout << "Test 4: ok" << std::endl;
  }

  {
    std::cout << "Test 5 started" << std::endl;
    Stack<int> s;
    for (int i = 1; i < 4; ++i) s.push(i);
    Stack<int> t;
    t = std::move(s);
    for (int i = 3; i > 0; --i) {
      assert(t.top() == i);
      assert(t.size() == static_cast<size_t>(i));
      t.pop();
    }
    std::cout << "Test 5: ok" << std::endl;
  }

  return 0;
}

